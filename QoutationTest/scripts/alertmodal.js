﻿const Toast = Swal.mixin({
    toast: true,
    position: 'top-end',
    showConfirmButton: false,
    timer: 3000
});
function success(message) {
    try {
        Toast.fire({
            type: 'success',
            title: " " +  message
        })
    } catch (ex) { console.log(ex.message) }
}
function info(message) {
    try {
        Toast.fire({
            type: 'info',
            title: message
        })
    } catch (ex) { console.log(ex.message) }
}
function warning(message) {
    debugger
    try {
        Toast.fire({
            type: 'warning',
            title: message
        })
    } catch (ex) { console.log(ex.message) }
}