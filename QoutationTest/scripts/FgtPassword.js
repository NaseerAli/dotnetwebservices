﻿function showFgtPwdDiv() {
    $("#Login").css('display', 'none');
    $("#Forgetpwd").css('display', 'block');

}

function GetPassword() {
    var email = $("#txt_frgtEmail").val();
    var data = {
        email: email
    }
    $.ajax({
        type: "POST",
        url: "webservices/General.asmx/GetPassword",
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                alert("Email Containing password sent to your registered account");

                window.location.reload();
            }
            else {

                alert("An error occured !!!");
            }
        },

    });

}