﻿$(function () {
    if (location.href.indexOf('?') != -1) {
        warning(GetQueryStringParams('error').replace(/%20/g, ' '))
    }
    getUserData();
});

function getUserData() {
    $.ajax({
        url: "webservices/GeneralHandler.asmx/GetUserData",
        type: "post",
        data: '',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            var result = (typeof data.d) == 'string' ? eval('(' + data.d + ')') : data.d;
            if (result.retCode == 1) {
                //$(window).unbind();
               // window.location.href = "index.aspx";

            }
        },
        error: function () {
            //AlertDanger('Error occured while logging out!');
            alert('Error occured while logging out!');
        }
    });
}


function GetQueryStringParams(sParam) {
    var sPageURL = window.location.search.substring(1);
    var sURLVariables = sPageURL.split('&');
    for (var i = 0; i < sURLVariables.length; i++) {
        var sParameterName = sURLVariables[i].split('=');
        if (sParameterName[0] == sParam) {
            return sParameterName[1];
        }
    }
}