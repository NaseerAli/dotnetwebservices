﻿$(document).ready(function () {
    GetCountry();
});
var arrCountry, arrCountryCity;

function GetCountry() {

    $.ajax({
        type: "POST",
        url: "../webservices/General.asmx/getCountry",
        data: {},
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                arrCountryCity = result.Country;
                $("#inputCountry").empty();
                var Country = '<option value="0">select Country</option>';
                if (arrCountryCity.length > 0)
                {
                    for (i = 0; i < arrCountryCity.length; i++) {
                        Country += '<option value=' + arrCountryCity[i].CountryCode + '>' + arrCountryCity[i].CountryName + '</option>';
                    }
                    $("#inputCountry").append(Country);
                    $("#SDA_inputCountry").append(Country);
                }
            }
        },
        error: function () {
            alert("An error occured while loading countries")
        }
    });
}

var arrCountryState;
function GetState(CountryCode) {
    
    var Data = { Code: CountryCode }
    debugger;
    $.ajax({
        type: "POST",
        url: "../webservices/General.asmx/getState",
        data: JSON.stringify(Data),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                $("#inputState").empty();
                arrCountryState = result.State;

                var State = '<option value="0">select state</option>';
                //var Country = '<option value="0">select Country</option>';

                if (arrCountryState.length > 0) {

                    for (i = 0; i < arrCountryState.length; i++) {
                        State += '<option value=' + arrCountryState[i].StateId + '>' + arrCountryState[i].StateName + '</option>';
                    }
                    $("#inputState").append(State);
                    $("#SDA_inputState").append(State);
                }
            }
        },
        error: function () {
            alert("An error occured while loading countries")
        }
    });

}

function GetCity(StateId) {
    //alert("Ji")
    // var CityCode = $("#inputCountry option:selected").text();
    var Data = { Id: StateId }
    debugger;
    $.ajax({
        type: "POST",
        url: "../webservices/General.asmx/getCity",
        data: JSON.stringify(Data),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                $("#inputCity").empty();
                arrCountryCity = result.City;

                var City = '<option value="0">select city</option>';
                //var Country = '<option value="0">select Country</option>';

                if (arrCountryCity.length > 0)
                {

                    for (i = 0; i < arrCountryCity.length; i++) {
                        City += '<option value=' + arrCountryCity[i].CityName + '>' + arrCountryCity[i].CityName + '</option>';
                    }
                    $("#inputCity").append(City);
                    $("#SDA_inputCity").append(City);
                }
            }
        },
        error: function () {
            alert("An error occured while loading countries")
        }
    });

}

function GetCountry1() {

    $.ajax({
        type: "POST",
        url: "productdetailHandler.asmx/GetCountry",
        data: {},
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                arrCountryCity = result.tbl_HCity;
                $("#SDA_inputCountry").empty();
                var Country = '<option value="0">select Country</option>';

                //$("#ddl_City").append(City);

                if (arrCountryCity.length > 0)
                {
                    //$("#selCountrynew").empty();

                    for (i = 0; i < arrCountryCity.length; i++) {
                        Country += '<option value=' + arrCountryCity[i].Countryname + '>' + arrCountryCity[i].Countryname + '</option>';
                    }
                   // $("#inputCountry").append(Country);
                    $("#SDA_inputCountry").append(Country);
                }
            }
        },
        error: function () {
            alert("An error occured while loading countries")
        }
    });
}

function GetCity1()
{
    //alert("Ji")
    var CountryName = $("#SDA_inputCountry option:selected").text();
    var Data = { Country: CountryName }
    debugger;
    $.ajax({
        type: "POST",
        url: "productdetailHandler.asmx/GetCity",
        data: JSON.stringify(Data),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                $("#SDA_inputCity").empty();
                arrCountryCity = result.tbl_HCity;

                var City = '<option value="0">select city</option>';
                //var Country = '<option value="0">select Country</option>';

                if (arrCountryCity.length > 0) {

                    for (i = 0; i < arrCountryCity.length; i++) {
                        City += '<option value=' + arrCountryCity[i].Description + '>' + arrCountryCity[i].Description + '</option>';
                    }
                  //  $("#inputCity").append(City);
                    $("#SDA_inputCity").append(City);
                }
            }
        },
        error: function () {
            alert("An error occured while loading countries")
        }
    });

}