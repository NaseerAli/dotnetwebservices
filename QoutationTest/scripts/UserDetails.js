﻿$(function () {
    GetUserData();
})
var arrUsers = new Array()
function GetUserData() {
    debugger
    $("#tbl_Users tbody").empty();

    post("WebServices/UserHandler.asmx/GetAllUserData", {}, function succes(data) {
        arrUsers = data.ListUser;
        $(data.ListUser).each(function (i,user) {
            debugger
            var tr = '';
            var j = i + 1;
            tr += '<tr>'
            tr += '<td>' + j + '</td>'
            tr += '<td>'+user.FullName+'</td>'
            tr += '<td>'+user.username+'</td>'
            tr += '<td>' + user.Email + '</td>'
            tr += '<td> <a style="cursor:pointer" onclick="ModelBoxShow(\'' + user.id + '\')"><i class="fa fa-edit" style="color:#008000"></i>&emsp;&emsp;'
            tr += '<a style="cursor:pointer" onclick="DeleteUser(\''+user.id+'\')"><i class="fa fa-trash " style="color:red"></i></td>'
            tr += '</tr>'
            $("#tbl_Users tbody").append(tr);
        });
        
    }, function error(data) {
        alert("Something wrong in loading user details.")
    });
}
function DeleteUser(id) {
    var arrUser = $.grep(arrUsers, function (p) { return p.id == id })
                                          .map(function (p) { return p; })[0];
    post("WebServices/UserHandler.asmx/DeleteSelectedUser", { arrUser: arrUser }, function success(data) {
        alert("Record deleted successfully...");
        GetUserData();
    }, function error(data) {
        alert("Something went wrong.")
    });
}

// Model Box 
function ModelBoxShow(id) {
    var arrUser = $.grep(arrUsers, function (p) { return p.id == id })
                                        .map(function (p) { return p; })[0];
    $("#txtfullname").val(arrUser.FullName);
    $("#txtEmail").val(arrUser.Email);
    $("#txtPassword").val(arrUser.password);
    $("#txtCon_Password").val(arrUser.password);
    $("#txt_id").val(id);
    $('#myModal').modal('show');
}
function ModelBoxHide() {
    $('#myModal').modal('hide');

}

// Update User Detail
function UpdateUser() {

    var data = {
        id: $("#txt_id").val(),
        FullName: $("#txtfullname").val(),
        password: $("#txtPassword").val(),
        Email: $("#txtEmail").val(),
        username: $("#txtEmail").val()
    }
    post("WebServices/UserHandler.asmx/UpdateSelectedUser", { user: data }, function () {
        ModelBoxHide();
        alert("Record updated succesfully...");
        GetUserData();
    }, function () {
        alert("Something went wrong.");
    })
}