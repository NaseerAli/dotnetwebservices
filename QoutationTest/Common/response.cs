﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;

namespace QoutationTest.Common
{
    public class response
    {
        static JavaScriptSerializer objSerializer = new JavaScriptSerializer();
        public  void write<T>(T arrData)
        {
            try
            {
                HttpContext.Current.Response.ContentType = "application/json";
                HttpContext.Current.Response.Write(objSerializer.Serialize(arrData));
                HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                HttpContext.Current.ApplicationInstance.CompleteRequest();
            }
            catch (Exception)
            {
                HttpContext.Current.Response.ContentType = "application/json";
                HttpContext.Current.Response.Write(objSerializer.Serialize(arrData));
                HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                HttpContext.Current.ApplicationInstance.CompleteRequest();
            }
        }
    }
}