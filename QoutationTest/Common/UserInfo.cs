﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace QoutationTest.Common
{
    public class UserInfo
    {
        [BsonRepresentation(BsonType.ObjectId)]
        public  string  id { get; set; }
        public string username { get; set; }
        public string password { get; set; }
        public string Email { get; set; }
        public string FullName { get; set; }
        public string Mobile { get; set; }
        public string Country { get; set; }
        public string State { get; set; }
        public string City { get; set; }
        public string Address { get; set; }
        public string Zipcode { get; set; }
        public string RoleId { get; set; }
    }
    public class Common
    {
        public static UserInfo objGlobal
        {
            get
            {
                if (HttpContext.Current.Session["loginUser"] == null)
                    throw new Exception("Invalid Session");
                return (UserInfo)HttpContext.Current.Session["loginUser"];
            }
        }
    }
}