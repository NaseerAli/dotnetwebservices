﻿using QoutationTest.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;

namespace QoutationTest.BL
{
    public class GeneralManager : IDisposable
    {
        public GeneralManager()
        {

        }
        public void Dispose()
        {
            //  memory re allocation;
            GC.SuppressFinalize(this);
        }

        public bool setUserDB
        {
            get
            {
                mongoHelper.currentCollection = "login_details";
                mongoHelper.currentdb = "punjani_live";
                return true;
            }
        }
        JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
        //var dbClient = new MongoClient("mongodb://localhost:27017");
        //IMongoDatabase db = dbClient.GetDatabase("punjani_live");
        //var coll = db.GetCollection<BsonDocument>("login_details");

        //public static mongoHelper.mbReturnCode Registeruser(string FullName, string Email, string Mobile, string Country, string State, string City, string Address, string Zipcode, string Password)
        //{
        //    mongoHelper.mbReturnCode retcode = mongoHelper.mbReturnCode.EXCEPTION;
        //    try
        //    {
        //        classes.mongoHelper.currentCollection = "login_details";
        //        classes.mongoHelper.currentdb = "punjani_live";
        //        var arrData = new List<object>();
        //        UserInfo objDefault = new UserInfo();

        //        var arrMatch = new { username = Email };
        //        retcode = mongoHelper._find(arrMatch, out arrData);

        //        if (retcode == mongoHelper.mbReturnCode.SUCCESSNORESULT)
        //        {

        //            var arrresult = new
        //            {
        //                FullName = FullName,
        //                Email = Email,
        //                username = Email,
        //                password = Password,
        //                Mobile = Mobile,
        //                City = City,
        //                Country = Country,
        //                State = State,
        //                Address = Address,
        //                Zipcode = Zipcode,
        //            };
        //            retcode = mongoHelper._InsertOne(arrresult);
        //            HttpContext.Current.Session["loginUser"] = new UserInfo
        //            {
        //                FullName = FullName,
        //                Email = Email,
        //                password = Password,
        //                Mobile = Mobile,
        //                City = City,
        //                Country = Country,
        //                State = State,
        //                Address = Address,
        //                Zipcode = Zipcode,

        //            };

        //        }
        //        else
        //        {
        //            throw new Exception("User Name already exist");
        //        }
        //    }
        //    catch (Exception ex)
        //    {

        //        throw new Exception(ex.Message);
        //    }

        //    return retcode;
        //}

        #region User Managment 

        public  mongoHelper.mbReturnCode Registeruser(UserInfo objDefault)
        {
            var retCode = mongoHelper.mbReturnCode.EXCEPTION;
            try
            {
                mongoHelper.currentCollection = "login_details";
                mongoHelper.currentdb = "punjani_live";
                var arrData = new List<object>();
                var arrMatch = new { username = objDefault.Email };
                retCode = mongoHelper._find(arrMatch, out arrData);
                if (retCode == mongoHelper.mbReturnCode.SUCCESSNORESULT)// Not  Exist
                {
                    var arrresult = new
                    {
                        FullName = objDefault.FullName,
                        Email = objDefault.Email,
                        username = objDefault.Email,
                        password = objDefault.password,
                        Mobile = objDefault.Mobile,
                        City = objDefault.City,
                        Country = objDefault.Country,
                        State = objDefault.State,
                        Address = objDefault.Address,
                        Zipcode = objDefault.Zipcode,
                        RoleId = objDefault.RoleId
                    };
                    retCode = mongoHelper._InsertOne(arrresult);
                    if (retCode == mongoHelper.mbReturnCode.SUCCESS)
                    {
                        var dynamic = arrresult;
                        var json = Newtonsoft.Json.JsonConvert.SerializeObject(dynamic);
                        HttpContext.Current.Session["loginUser"] = objDefault;
                    }

                }
                else if (retCode == mongoHelper.mbReturnCode.SUCCESS)// Already Exist
                {
                    throw new Exception("user name or Email already exist ,please try another!");
                }
                else
                {
                    //  json = jsSerializer.Serialize(new { retCode = 0, message = "user name or password is incorrect" });
                }

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                Dispose();
            }
            return retCode;

        }

        //public static string GetUserData()
        //{
        //    JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
        //    string json = "";
        //    try
        //    {

        //        //   var arrresult = new List<UserInfo>();
        //        var arrdata = HttpContext.Current.Session["loginUser"];
        //        if (arrdata != null)
        //        {
        //            json = jsSerializer.Serialize(new { retCode = 1, data = arrdata });
        //        }

        //    }
        //    catch (Exception ex)
        //    {

        //        throw new Exception(ex.Message);
        //    }

        //    return json;
        //}

        //public static string UserLogin(string UserName, string Password)
        //{
        //    JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
        //    string json = "";
        //    try
        //    {
        //        classes.mongoHelper.currentCollection = "login_details";
        //        classes.mongoHelper.currentdb = "punjani_live";
        //        var arrData = new List<object>();
        //        UserInfo objDefault = new UserInfo();
        //        var arrMatch = new { username = UserName, password = Password };
        //        var _mdretCode = mongoHelper._find(arrMatch, out arrData);
        //        if (_mdretCode == mongoHelper.mbReturnCode.SUCCESS)
        //        {
        //            var dynamic = arrData[0];
        //            var arrresult = Newtonsoft.Json.JsonConvert.SerializeObject(dynamic);
        //            //   arruser = Newtonsoft.Json.JsonConvert.DeserializeObject<dynamic>(json);
        //            HttpContext.Current.Session["loginUser"] = arrresult;
        //            // var arrrdata = HttpContext.Current.Session["loginUser"];

        //            json = jsSerializer.Serialize(new { retCode = 1, data = arrresult });
        //        }
        //        else
        //        {
        //            json = jsSerializer.Serialize(new { retCode = 0, message = "user name or password is incorrect" });
        //        }

        //    }
        //    catch (Exception ex)
        //    {
        //        throw new Exception(ex.Message);
        //    }

        //    return json;
        //}


        //public static string UpdateProfile(string FullName, string Email, string Mobile, string Country, string State, string City, string Address, string Zipcode, string Password, string RoleId)
        //{
        //    string json = "";
        //    JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
        //    try
        //    {
        //        classes.mongoHelper.currentCollection = "login_details";
        //        classes.mongoHelper.currentdb = "punjani_live";
        //        var arrData = new List<object>();
        //        var arrMatch = new { username = Email };
        //        var retcode = mongoHelper._find(arrMatch, out arrData);
        //        if (retcode == mongoHelper.mbReturnCode.SUCCESS)
        //        {
        //            var arrresult = new
        //            {
        //                FullName = FullName,
        //                Email = Email,
        //                username = Email,
        //                password = Password,
        //                Mobile = Mobile,
        //                City = City,
        //                Country = Country,
        //                State = State,
        //                Address = Address,
        //                Zipcode = Zipcode,
        //                RoleId = RoleId
        //            };
        //            retcode = mongoHelper._update(arrresult, arrData);
        //            if (retcode == mongoHelper.mbReturnCode.SUCCESS)
        //            {
        //                json = jsSerializer.Serialize(new { retCode = 1, message = "User Profile Updated Successfully!!" });
        //            }
        //        }


        //    }
        //    catch (Exception ex)
        //    {

        //        throw new Exception(ex.Message);
        //    }

        //    return json;
        //}


        #endregion

        //#region Registration Mail To Customer
        //public static bool RegistrationMail(string Email, string Password)
        //{
        //    try
        //    {

        //        TemplateGenrator.GetTemplatePath("Registration");
        //        String Mailhtml = "";
        //        TemplateGenrator.arrParms = new Dictionary<string, string> {
        //                 {"_UserName",Email },
        //                 {"_Password",Password},
        //                 {"_CustomerEmail",Email}
        //                 };
        //        Mailhtml += TemplateGenrator.GetTemplate;
        //        List<string> from = new List<string>();
        //        from.Add(Convert.ToString(ConfigurationManager.AppSettings["supportMail"]));
        //        List<string> DocLinksList = new List<string>();
        //        Dictionary<string, string> Email1List = new Dictionary<string, string>();

        //        foreach (string mail in Email.Split(','))
        //        {
        //            if (mail != "")
        //            {
        //                Email1List.Add(mail, mail);
        //            }
        //        }
        //        string accessKey = Convert.ToString(ConfigurationManager.AppSettings["AccessKey"]);
        //        bool reponse = MailManager.SendMail(accessKey, Email1List, "Your login Details ", Mailhtml, from, DocLinksList);
        //        return reponse;
        //    }
        //    catch (Exception ex)
        //    {

        //        return false;
        //    }
        //}
        //#endregion



        //#region Forgot Mail 
        //public static string ForgetPass(string email)
        //{
        //    string json = "";
        //    try
        //    {
        //        classes.mongoHelper.currentCollection = "login_details";
        //        classes.mongoHelper.currentdb = "punjani_live";
        //        var arrData = new List<object>();
        //        UserInfo objDefault = new UserInfo();
        //        var arrMatch = new { username = email };
        //        var _mdretCode = mongoHelper._find(arrMatch, out arrData);

        //        if (_mdretCode == mongoHelper.mbReturnCode.SUCCESS)
        //        {
        //            var dynamic = arrData[0];
        //            var arrresult = Newtonsoft.Json.JsonConvert.SerializeObject(dynamic);
        //            //var Password = arrresult[0].password;
        //            //  ForgotMail(email, Password);
        //            json = "{\"Session\":\"1\",\"retCode\":\"1\"}";
        //        }
        //        else
        //            json = "{\"Session\":\"1\",\"retCode\":\"2\"}";

        //    }
        //    catch (Exception ex)
        //    {

        //        json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
        //    }
        //    return json;

        //}


        //public static bool ForgotMail(string Email, string Password)
        //{
        //    try
        //    {
        //        TemplateGenrator.GetTemplatePath("Registration");
        //        String Mailhtml = "";
        //        TemplateGenrator.arrParms = new Dictionary<string, string> {
        //                 {"_UserName",Email },
        //                 {"_Password",Password},
        //                 {"_CustomerEmail",Email}
        //                 };
        //        Mailhtml += TemplateGenrator.GetTemplate;
        //        List<string> from = new List<string>();
        //        from.Add(Convert.ToString(ConfigurationManager.AppSettings["supportMail"]));
        //        List<string> DocLinksList = new List<string>();
        //        Dictionary<string, string> Email1List = new Dictionary<string, string>();

        //        foreach (string mail in Email.Split(','))
        //        {
        //            if (mail != "")
        //            {
        //                Email1List.Add(mail, mail);
        //            }
        //        }
        //        string accessKey = Convert.ToString(ConfigurationManager.AppSettings["AccessKey"]);
        //        bool reponse = MailManager.SendMail(accessKey, Email1List, "Your Wogo Naturals login Details ", Mailhtml, from, DocLinksList);
        //        return reponse;
        //    }
        //    catch (Exception ex)
        //    {

        //        return false;
        //    }
        //}
        //#endregion


        //public static object UserLogin(string UserName, string Password)
        //{
        //    object arruser = new object();
        //    try
        //    {
        //        classes.mongoHelper.currentCollection = "login_details";
        //        classes.mongoHelper.currentdb = "punjani_live";
        //        var arrData = new List<object>();
        //        UserInfo objDefault = new UserInfo();
        //        HttpContext.Current.Session["loginUser"] = "";


        //        var arrMatch = new { username = UserName, password = Password };

        //        var arrdata = mongoHelper.arrCollection.FirstOrDefault();
        //        var _mdretCode = mongoHelper._find(arrMatch, out arrData);
        //        if (_mdretCode == mongoHelper.mbReturnCode.SUCCESS)
        //        {
        //            var dynamic = arrData[0];
        //            string json = Newtonsoft.Json.JsonConvert.SerializeObject(dynamic);
        //            arruser = Newtonsoft.Json.JsonConvert.DeserializeObject<dynamic>(json);
        //            HttpContext.Current.Session["loginUser"] = arrData[0];
        //            // var arrrdata = HttpContext.Current.Session["loginUser"];
        //        }
        //        else
        //            throw new Exception("User profile not matched");
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new Exception(ex.Message);
        //    }
        //    return arruser;
        //}




    }
}