﻿using QoutationTest.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;
using QoutationTest.org;
using QoutationTest.BL;

namespace QoutationTest.WebServices
{
    /// <summary>
    /// Summary description for UserHandler
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class UserHandler : System.Web.Services.WebService
    {
        JavaScriptSerializer objUserData = new JavaScriptSerializer();
        private response objResponse = new response();

        [WebMethod(EnableSession =true)]
        public string GetAllUserData()
        {
            try
            {
                var objClass = new BL.ManageUser();
                var ListUser = new List<Common.UserInfo>();
                var retCode = objClass.GetUserList(out ListUser);
                return objUserData.Serialize(new { retCode=1,ListUser });
            }
            catch (Exception ex)
            {
                return objUserData.Serialize(new { retCode = 0, Message =ex.Message }); 
            }
        }

        [WebMethod(EnableSession = true)]
        public string UpdateSelectedUser(UserInfo user)
        {
            try
            {
                var objClass = new BL.ManageUser();
                var retCode = objClass.UpdateUser(user);
                return objUserData.Serialize(new { retCode = 1 });
            }
            catch (Exception ex)
            {

                return objUserData.Serialize(new { retCode = 0, Message = ex.Message });
            }
        }

        [WebMethod(EnableSession = true)]
        public string DeleteSelectedUser(UserInfo arrUser)
        {
            try
            {
                var objClass = new BL.ManageUser();
                var retCode = objClass.DeleteUser(arrUser);
                return objUserData.Serialize(new { retCode = 1 });
            }
            catch (Exception ex)
            {
                return objUserData.Serialize(new { retCode = 0, Message = ex.Message });
            }
        }


        [WebMethod(EnableSession = true)]
        public void JSonDelete(UserInfo arrUser)
        {
            try
            {
                var objClass = new BL.ManageUser();
                var retCode = objClass.DeleteUser(arrUser);
                objResponse.write(new { retCode = 1 });
            }
            catch (Exception ex)
            {
                objResponse.write(new { retCode = 0, Message = ex.Message });
            }
        }

        [WebMethod(EnableSession = true)]
        public void JSonUpdate(UserInfo user)
        {
            try
            {
                var objClass = new BL.ManageUser();
                var retCode = objClass.UpdateUser(user);
                objResponse.write(new { retCode = 1 });
            }
            catch (Exception ex)
            {
                objResponse.write(new { retCode = 0, Message = ex.Message });
            }
        }

        [WebMethod(EnableSession = true)]
        public void JSonRegisterUser(UserInfo arrUser)
        {
            try
            {
                var objUser = new GeneralManager();
                var retCode = objUser.Registeruser(arrUser);
                //response.write<object(new { retCode = retCode });
                objResponse.write(new { retCode = retCode });//

            }
            catch (Exception ex)
            {
                //response.write<object>(new { retCode = 0 });
                objResponse.write(new { retCode = 0, errorMessage = ex.Message });
            }
        }

        [WebMethod(EnableSession = true)]
        public void JsonGetAllUserData()
        {
            try
            {
                var objClass = new BL.ManageUser();
                var ListUser = new List<Common.UserInfo>();
                var retCode = objClass.GetUserList(out ListUser);
                objResponse.write(new { retCode = 1, ListUser });
            }
            catch (Exception ex)
            {
                objResponse.write(new { retCode = 0, Message = ex.Message });
            }
        }
    }
}
